import React from "react";
import { theme } from "./theme";
import { Props } from "./props";
import { useStyles } from "./styles";
import { ThemeProvider, Grid, Typography } from "@material-ui/core";
import Translator from "../resources/translator";

const Component: React.FC<Props> = (props) => {
    const classes = useStyles(props);
    return (
        <ThemeProvider theme={theme}>
            <Grid container spacing={2} className={classes.grid}>
                <Grid item xs={12} className={classes.title}>
                    <Typography variant="h2">ترجمه به زبان پارسی</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Translator />
                </Grid>
            </Grid>
        </ThemeProvider>
    );
};
export default Component;
