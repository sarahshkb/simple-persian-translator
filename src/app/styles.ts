import { createStyles, makeStyles } from "@material-ui/core";
import { Props } from "./props";
import Image from "../assets/images/pasargad1.jpg";

export const useStyles = (props: Props) => {
    return makeStyles((theme) =>
        createStyles({
            grid: {
                height: "100vh",
                backgroundImage: `url(${Image})`,
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            },
            title: {
                textAlign: "center",
                marginTop: "2%",
            },
        })
    )(props);
};
