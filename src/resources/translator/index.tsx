import React from "react";
import { useStyles } from "./styles";
import { Props } from "./props";
import {
    Container,
    TextField,
    InputAdornment,
    Typography,
    Grid,
} from "@material-ui/core";
import TranslateIcon from "@material-ui/icons/Translate";
import axios from "axios";

const Component: React.FC<Props> = (props) => {
    const classes = useStyles(props);
    const [translated, setTranslated] = React.useState("");
    const translationRequests = React.useRef(0);
    const handleChange = async (event: any) => {
        try {
            translationRequests.current += 1;
            let result = await axios({
                method: "POST",
                url: "https://rapidapi.p.rapidapi.com/translate",
                params: {
                    to: "fa",
                    "api-version": "3.0",
                    profanityAction: "NoAction",
                    textType: "plain",
                },
                headers: {
                    "content-type": "application/json",
                    "x-rapidapi-host":
                        "microsoft-translator-text.p.rapidapi.com",
                    "x-rapidapi-key":
                        "055bfdf06fmsh83a4ef9b5f553d2p1f59a4jsn043ee726383e",
                },
                data: [
                    {
                        Text: event.target.value,
                    },
                ],
            });

            translationRequests.current -= 1;
            console.log(translationRequests.current);
            if (translationRequests.current > 0) {
                return;
            }
            setTranslated(result.data[0].translations[0].text);
        } catch (error) {
            console.log(error);
            setTranslated("خطا در دریافت داده");
        }
    };

    return (
        <Container maxWidth="sm" className={classes.container}>
            <Grid container spacing={2}>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <TranslateIcon />
                                </InputAdornment>
                            ),
                        }}
                        margin="normal"
                        inputProps={{ style: { fontSize: 30 } }}
                        onChange={handleChange}
                        className={classes.textfield}
                    />
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <Typography
                        variant="h5"
                        dir="rtl"
                        className={classes.typography}
                    >
                        {translated}
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Component;
