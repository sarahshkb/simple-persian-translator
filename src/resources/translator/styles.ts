import { createStyles, makeStyles } from "@material-ui/core";
import { Props } from "./props";

export const useStyles = (props: Props) => {
    return makeStyles((theme) =>
        createStyles({
            container: {
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                paddingTop: "5%",
                paddingBottom: "5%",
                background:
                    "radial-gradient(circle, rgba(238,174,202,1) 0%, rgba(148,187,233,1) 100%)",
                borderRadius: 15,
            },
            textfield: {
                backgroundColor: "white",
                borderRadius: 10,
                border: 0,
            },
            typography: {
                margin: "2%",
                fontSize: 40,
            },
            gridItem: {
                textAlign: "center",
            },
        })
    )(props);
};
